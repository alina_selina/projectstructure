﻿using Projects.Common.DTO;
using System.Collections.Generic;
using Projects.Common.DTO.Query;
using Projects.Common.DTO.Task;
using CollectionsAndLINQ.Entities;

namespace Projects.BLL.Interfaces
{
    public interface IQueryService
    {
        IEnumerable<ProjectTasksDTO> GetProjectTaskCountByUserId(int userId);

        IEnumerable<TaskDTO> GetUserTasksByUserId(int userId);

        IEnumerable<UserTasksCurrentYearDTO> GetUserTasksCurrentYearByUserId(int userId);

        IEnumerable<TeamsDTO> GetTeams();

        IEnumerable<UsersDTO> GetUsers();

        UserStructDTO GetUserStruct(int userId);

        IEnumerable<ProjectStructDTO> GetProjectStruct();
    }
}
