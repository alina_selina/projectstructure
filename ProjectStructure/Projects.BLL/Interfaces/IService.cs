﻿using System.Collections.Generic;

namespace Projects.BLL.Interfaces
{
    public interface IService<TDto, TCreateDto> where TDto : class where TCreateDto : class
    {
        IEnumerable<TDto> GetAll();

        TDto GetById(object id);

        TDto Create(TCreateDto dto);

        TDto Update(TDto dto);

        void Delete(object id);
    }
}
