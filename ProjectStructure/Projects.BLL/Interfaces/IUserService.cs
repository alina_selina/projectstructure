﻿using Projects.Common.DTO;
using Projects.Common.DTO.User;

namespace Projects.BLL.Interfaces
{
    public interface IUserService : IService<UserDTO, UserCreateDTO>
    {
    }
}
