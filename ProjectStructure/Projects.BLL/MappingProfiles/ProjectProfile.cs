﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Projects.Common.DTO;

namespace Projects.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectCreateDTO, Project>();

            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>();
        }
    }
}
