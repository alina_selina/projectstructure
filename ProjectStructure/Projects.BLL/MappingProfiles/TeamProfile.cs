﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Projects.Common.DTO.Team;

namespace Projects.BLL.MappingProfiles
{ 
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {

            CreateMap<TeamCreateDTO, Team>();

            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();
        }
    }
}
