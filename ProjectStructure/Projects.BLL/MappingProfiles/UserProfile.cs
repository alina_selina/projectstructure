﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Projects.Common.DTO;
using Projects.Common.DTO.User;

namespace Projects.BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserCreateDTO, User>();

            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();
        }
    }
}
