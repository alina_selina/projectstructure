﻿using AutoMapper;
using Projects.DAL.Interfaces;

namespace Projects.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        protected readonly IUnitOfWork _uow;
        protected readonly IMapper _mapper;

        public BaseService(IUnitOfWork uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }
    }
}
