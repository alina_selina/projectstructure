﻿using AutoMapper;
using Projects.BLL.Interfaces;
using Projects.DAL.Interfaces;
using System.Collections.Generic;

namespace Projects.BLL.Services.Abstract
{
    public abstract class Service<TDto, TCreateDto> : BaseService,  IService<TDto, TCreateDto> where TDto : class where TCreateDto : class
    {
        public Service(IUnitOfWork provider, IMapper mapper):base(provider, mapper)
        {
        }

        public abstract TDto Create(TCreateDto dto);

        public abstract void Delete(object id);

        public abstract IEnumerable<TDto> GetAll();

        public abstract TDto GetById(object id);

        public abstract TDto Update(TDto dto);
    }
}
