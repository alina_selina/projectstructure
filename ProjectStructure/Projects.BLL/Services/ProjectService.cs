﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Projects.BLL.Interfaces;
using Projects.BLL.Exceptions;
using Projects.BLL.Services.Abstract;
using Projects.Common.DTO;
using Projects.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Projects.BLL.Services
{
    public class ProjectService : Service<ProjectDTO, ProjectCreateDTO>, IProjectService
    {
        public ProjectService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        {
        }

        public override ProjectDTO Create(ProjectCreateDTO dto)
        {
            var project = _mapper.Map<Project>(dto);
            _uow.GetRepository<Project>().Create(project);

            return _mapper.Map<ProjectDTO>(project);
        }

        public override ProjectDTO Update(ProjectDTO dto)
        {
            var project = _mapper.Map<Project>(dto);
            var updatedProject = _uow.GetRepository<Project>().Update(project);

            if (updatedProject == null)
                throw new NotFoundException(nameof(Project), project.Id.ToString());

            return _mapper.Map<ProjectDTO>(updatedProject);
        }

        public override IEnumerable<ProjectDTO> GetAll()
        {
            var projects = _uow.GetRepository<Project>().GetAll();

            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }

        public override void Delete(object id)
        {
            var project = _uow.GetRepository<Project>().GetFirstOrDefault(p => p.Id.Equals(id));

            if (project == null)
                throw new NotFoundException(nameof(Project), id.ToString());

            _uow.GetRepository<Project>().Delete(id);
        }

        public override ProjectDTO GetById(object id)
        {
            var project = _uow.GetRepository<Project>().GetFirstOrDefault(u => u.Id.Equals(id));

            if (project == null)
                throw new NotFoundException(nameof(Project), id.ToString());

            return _mapper.Map<ProjectDTO>(project);
        }
    }
}
