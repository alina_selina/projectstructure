﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Projects.BLL.Interfaces;
using Projects.BLL.Services.Abstract;
using Projects.Common.DTO;
using Projects.Common.DTO.Query;
using Projects.Common.DTO.Task;
using Projects.Common.DTO.User;
using Projects.Common.Enums;
using Projects.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Projects.BLL.Services
{
    public class QueryService : BaseService, IQueryService
    {
        private readonly IEnumerable<Project> _projects;

        public QueryService(IUnitOfWork provider, IMapper mapper) : base(provider, mapper)
        {
            _projects = _uow.GetRepository<Project>().GetAll();
        }

        /*Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь проект:кол-во тасков).*/
        public IEnumerable<ProjectTasksDTO> GetProjectTaskCountByUserId(int userId)
        {
            return _projects.SelectMany(p => p.Tasks, (project, tasks) => new { project, tasks })
                .Where(projectTasks => projectTasks.tasks.Performer.Id == userId)
                .Select(p => new ProjectTasksDTO()
                {
                    Project = _mapper.Map<ProjectDTO>(p.project), 
                    TaskCount = p.project.Tasks.Count
                });
        }

        /*Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов (коллекция тасков).*/
        public IEnumerable<TaskDTO> GetUserTasksByUserId(int userId)
        {
            return _mapper.Map<List<TaskDTO>>(
                _projects.SelectMany(p => p.Tasks.Where(t => t.Performer.Id == userId && t.Name.Length < 45)));
        }

        /*Получить список(id, name) из коллекции тасков, которые выполнены(finished) в текущем(2021) году для конкрет. пользователя (по id).*/
        public IEnumerable<UserTasksCurrentYearDTO> GetUserTasksCurrentYearByUserId(int userId)
        {
            return _projects.SelectMany(p => p.Tasks.Where(t => t.Performer.Id == userId && t.FinishedAt?.Year == DateTime.Now.Year))
                .Select(t => new UserTasksCurrentYearDTO() 
                { 
                    Id = t.Id, 
                    Name = t.Name 
                });
        }

        /* Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет, 
         * отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.*/
        public IEnumerable<TeamsDTO> GetTeams()
        {
            //только те команды, в которых все участники старше 10 лет
            return _projects.GroupBy(u => u.Team.Id, u => new { u.Team.Id, u.Team.Name, u.Team.Users })
                .Where(g => g.Select(t => t.Users.All(u => (DateTime.Now.Year - u.BirthDay.Year) > 10)).First())
                .Select(group => new TeamsDTO()
                {
                    Id = group.Select(g => g.Id).First(),
                    Name = group.Select(g => g.Name).First(),
                    Users = _mapper.Map<List<UserDTO>>(group.Select(g => g.Users.OrderByDescending(u => u.RegisteredAt)).Distinct().First().ToList())
                });
        }

        /*Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).*/
        public IEnumerable<UsersDTO> GetUsers()
        {
            return _projects.SelectMany(p => p.Tasks)
                .GroupBy(u => u.Performer.Id, t => new { Task = t, t.Performer })
                .Select(group => new UsersDTO()
                {
                    User = _mapper.Map<UserDTO>(group.Select(g => g.Performer).First()),
                    Tasks = _mapper.Map<List<TaskDTO>>(group.OrderByDescending(u => u.Task.Name.Length).Select(g => g.Task).ToList())
                })
                .OrderBy(u => u.User.FirstName);
        }

        /*Получить следующую структуру (передать Id пользователя в параметры):
            User
            Последний проект пользователя (по дате создания)
            Общее кол-во тасков под последним проектом
            Общее кол-во незавершенных или отмененных тасков для пользователя
            Самый долгий таск пользователя по дате (раньше всего создан - позже всего закончен)*/
        public UserStructDTO GetUserStruct(int userId)
        {
            return (from p in _projects
                    from t in p.Tasks
                    where t.Performer.Id == userId
                    let lp = _projects.SelectMany(p => p.Team.Users.Where(t => t.Id == userId), (project, users) => new { project })
                                      .OrderByDescending(p => p.project.CreatedAt)
                                      .FirstOrDefault().project
                    let tsks = _projects.SelectMany(p => p.Tasks.Where(t => t.Performer.Id == userId))
                    select new UserStructDTO()
                    {
                        User = _mapper.Map<UserDTO>(t.Performer),
                        LastProject = _mapper.Map<ProjectDTO>(lp),
                        LastProjectTaskCount = lp.Tasks.Count,
                        UnfinishedTaskCount = tsks.Where(t => t.State == TaskState.InProgress || t.State == TaskState.Canceled).ToList().Count,
                        LongestTask = _mapper.Map<TaskDTO>(tsks.OrderByDescending(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt).FirstOrDefault())
                    })
                    .FirstOrDefault();
        }

        /*Получить следующую структуру:
            Проект
            Самый длинный таск проекта (по описанию)
            Самый короткий таск проекта (по имени)
            Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол-во тасков < 3*/
        public IEnumerable<ProjectStructDTO> GetProjectStruct()
        {
            return _projects.Where(p => p.Description.Length > 20 || p.Tasks.Count < 3)
                .Select(p => new ProjectStructDTO()
                {
                    Project = _mapper.Map<ProjectDTO>(p),
                    LongestTask = _mapper.Map<TaskDTO>(p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault()),
                    ShortestTask = _mapper.Map<TaskDTO>(p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault()),
                    UserCount = p.Team.Users.Count
                });
        }
    }
}
