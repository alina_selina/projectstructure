﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Projects.BLL.Interfaces;
using Projects.BLL.Exceptions;
using Projects.BLL.Services.Abstract;
using Projects.Common.DTO.Task;
using Projects.DAL.Interfaces;
using System.Collections.Generic;

namespace Projects.BLL.Services
{
    public class TaskService : Service<TaskDTO, TaskCreateDTO>, ITaskService
    {
        public TaskService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        {
        }

        public override TaskDTO Create(TaskCreateDTO dto)
        {
            var task = _mapper.Map<Task>(dto);
            _uow.GetRepository<Task>().Create(task);

            return _mapper.Map<TaskDTO>(task);
        }

        public override TaskDTO Update(TaskDTO dto)
        {
            var task = _mapper.Map<Task>(dto);
            var updatedTask = _uow.GetRepository<Task>().Update(task);

            if (updatedTask == null)
                throw new NotFoundException(nameof(Task), task.Id.ToString());

            return _mapper.Map<TaskDTO>(updatedTask);
        }

        public override IEnumerable<TaskDTO> GetAll()
        {
            var tasks = _uow.GetRepository<Task>().GetAll();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

        public override void Delete(object id)
        {
            var task = _uow.GetRepository<Task>().GetFirstOrDefault(t => t.Id.Equals(id));

            if (task == null)
                throw new NotFoundException(nameof(Task), id.ToString());

            _uow.GetRepository<Task>().Delete(id);
        }

        public override TaskDTO GetById(object id)
        {
            var task = _uow.GetRepository<Task>().GetFirstOrDefault(u => u.Id.Equals(id));

            if (task == null)
                throw new NotFoundException(nameof(Task), id.ToString());

            return _mapper.Map<TaskDTO>(task);
        }
    }
}
