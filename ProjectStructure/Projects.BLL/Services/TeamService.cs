﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Projects.BLL.Interfaces;
using Projects.BLL.Exceptions;
using Projects.BLL.Services.Abstract;
using Projects.Common.DTO.Team;
using Projects.DAL.Interfaces;
using System.Collections.Generic;

namespace Projects.BLL.Services
{
    public class TeamService : Service<TeamDTO, TeamCreateDTO>, ITeamService
    {
        public TeamService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        {
        }

        public override TeamDTO Create(TeamCreateDTO dto)
        {
            var team = _mapper.Map<Team>(dto);
            _uow.GetRepository<Team>().Create(team);

            return _mapper.Map<TeamDTO>(team);
        }

        public override TeamDTO Update(TeamDTO dto)
        {
            var team = _mapper.Map<Team>(dto);
            var updatedTeam = _uow.GetRepository<Team>().Update(team);

            if (updatedTeam == null)
                throw new NotFoundException(nameof(Team), team.Id.ToString());

            return _mapper.Map<TeamDTO>(updatedTeam);
        }

        public override IEnumerable<TeamDTO> GetAll()
        {
            var teams = _uow.GetRepository<Team>().GetAll();

            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }

        public override void Delete(object id)
        {
            var team = _uow.GetRepository<Team>().GetFirstOrDefault(t => t.Id.Equals(id));

            if (team == null)
                throw new NotFoundException(nameof(Team), id.ToString());

            _uow.GetRepository<Team>().Delete(id);
        }

        public override TeamDTO GetById(object id)
        {
            var team = _uow.GetRepository<Team>().GetFirstOrDefault(u => u.Id.Equals(id));

            if (team == null)
                throw new NotFoundException(nameof(Team), id.ToString());

            return _mapper.Map<TeamDTO>(team);
        }
    }
}
