﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Projects.BLL.Exceptions;
using Projects.BLL.Interfaces;
using Projects.BLL.Services.Abstract;
using Projects.Common.DTO;
using Projects.Common.DTO.User;
using Projects.DAL.Interfaces;
using System.Collections.Generic;

namespace Projects.BLL.Services
{
    public class UserService : Service<UserDTO, UserCreateDTO>, IUserService
    {
        public UserService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        {
        }

        public override UserDTO Create(UserCreateDTO dto)
        {
            var user = _mapper.Map<User>(dto);
            _uow.GetRepository<User>().Create(user);

            return _mapper.Map<UserDTO>(user);
        }

        public override UserDTO Update(UserDTO dto)
        {
            var user = _mapper.Map<User>(dto);
            var updatedUser = _uow.GetRepository<User>().Update(user);

            if (updatedUser == null)
                throw new NotFoundException(nameof(User), user.Id.ToString());

            return _mapper.Map<UserDTO>(updatedUser);
        }

        public override IEnumerable<UserDTO> GetAll()
        {
            var users = _uow.GetRepository<User>().GetAll();

            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }

        public override void Delete(object id)
        {
            var user = _uow.GetRepository<User>().GetFirstOrDefault(u => u.Id.Equals(id));

            if (user == null)
                throw new NotFoundException(nameof(User), id.ToString());

            _uow.GetRepository<User>().Delete(id);
        }

        public override UserDTO GetById(object id)
        {
            var user = _uow.GetRepository<User>().GetFirstOrDefault(u => u.Id.Equals(id));

            if (user == null)
                throw new NotFoundException(nameof(User), id.ToString());

            return _mapper.Map<UserDTO>(user);
        }
    }
}
