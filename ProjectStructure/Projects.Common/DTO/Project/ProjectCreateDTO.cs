﻿using Projects.Common.DTO.Team;
using Projects.Common.DTO.User;
using System;

namespace Projects.Common.DTO
{
    public class ProjectCreateDTO
    {
        public UserDTO Author { get; set; }
        public TeamDTO Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
