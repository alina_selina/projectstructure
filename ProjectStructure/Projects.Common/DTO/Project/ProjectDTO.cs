﻿using System;
using Projects.Common.DTO.Team;
using Projects.Common.DTO.User;

namespace Projects.Common.DTO
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public UserDTO Author { get; set; }
        public TeamDTO Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
