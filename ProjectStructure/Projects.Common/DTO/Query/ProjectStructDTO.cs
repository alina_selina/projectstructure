﻿using Projects.Common.DTO.Task;

namespace Projects.Common.DTO.Query
{
    public class ProjectStructDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTask { get; set; }
        public TaskDTO ShortestTask { get; set; }
        public int UserCount { get; set; }
    }
}
