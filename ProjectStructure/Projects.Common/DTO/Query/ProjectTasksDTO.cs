﻿namespace Projects.Common.DTO.Query
{
    public class ProjectTasksDTO
    {
        public ProjectDTO Project { get; set; }
        public int TaskCount { get; set; }
    }
}
