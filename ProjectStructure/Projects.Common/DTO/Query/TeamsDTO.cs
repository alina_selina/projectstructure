﻿using Projects.Common.DTO.User;
using System.Collections.Generic;

namespace Projects.Common.DTO.Query
{
    public class TeamsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserDTO> Users { get; set; }
    }
}
