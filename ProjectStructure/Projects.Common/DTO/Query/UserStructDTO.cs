﻿using Projects.Common.DTO.Task;
using Projects.Common.DTO.User;

namespace Projects.Common.DTO.Query
{
    public class UserStructDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int LastProjectTaskCount { get; set; }
        public int UnfinishedTaskCount { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}
