﻿using Projects.Common.DTO.User;
using Projects.Common.Enums;
using System;

namespace Projects.Common.DTO.Task
{
    public class TaskCreateDTO
    {
        public int ProjectId { get; set; }
        public UserDTO Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
