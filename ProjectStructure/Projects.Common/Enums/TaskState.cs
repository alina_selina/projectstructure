﻿namespace Projects.Common.Enums
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}
