﻿namespace Projects.Common.Interfaces
{
    public interface IIdGeneratable
    {
        int GetGeneratedId();
    }
}
