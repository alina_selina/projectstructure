﻿using Projects.ConsoleClient.Interfaces;
using System;

namespace Projects.ConsoleClient.Commands
{
    class Command : ICommand
    {
        private Action Action { get; }

        public Command(Action action)
        {
            Action = action;
        }

        public void Execute()
        {
            Action.Invoke();
        }
    }
}
