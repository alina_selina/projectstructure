﻿using Projects.ConsoleClient.Interfaces;
using static System.Console;
using static Projects.ConsoleClient.Utils.ConsoleUtils;
using static Projects.ConsoleClient.Validators.IdValidator;
using Projects.ConsoleClient.DTO;
using Newtonsoft.Json;
using System.Collections.Generic;
using Projects.ConsoleClient.Decorators;

namespace Projects.ConsoleClient.Commands
{
    class ShowProjectTaskCountByUserId : HttpCommand
    {
        public ShowProjectTaskCountByUserId(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public ShowProjectTaskCountByUserId(IHttpService httpService, string path, int timeout): base(httpService, path, timeout)
        {
        }

        [TaskCanceledHandler]
        [HttpErrorHandler]
        public override void Execute()
        {
            if (!TryGetIdInput(out int id))
                return;

            var response = Wait(() => Client.Get($"{Path}/{id}", Timeout).GetAwaiter().GetResult());
            if (response != null && !response.IsSuccessStatusCode)
            {
                ShowHttpResponse<HttpErrorResponse>(response);
                return;
            }

            var result = JsonConvert.DeserializeObject<List<ProjectTasksDTO>>(response.Content.ReadAsStringAsync().Result);

            WriteLine("\nResult:\n");
            if (result.Count > 0)
                result.ForEach(v => WriteLine(v.ToString()));
            else
                WriteLine("Not yet");

            WaitForAnyKeyPress();
        }
    }
}
