﻿using Projects.ConsoleClient.Interfaces;
using static System.Console;
using static Projects.ConsoleClient.Utils.ConsoleUtils;
using Projects.ConsoleClient.DTO;
using Newtonsoft.Json;
using System.Collections.Generic;
using Projects.ConsoleClient.Decorators;

namespace Projects.ConsoleClient.Commands
{
    class ShowTeams : HttpCommand
    {
        public ShowTeams(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public ShowTeams(IHttpService httpService, string path, int timeout) : base(httpService, path, timeout)
        {
        }

        [TaskCanceledHandler]
        [HttpErrorHandler]
        public override void Execute()
        {
            var response = Wait(() => Client.Get(Path, Timeout).GetAwaiter().GetResult());
            if (response != null && !response.IsSuccessStatusCode)
            {
                ShowHttpResponse<HttpErrorResponse>(response);
                return;
            }

            var result = JsonConvert.DeserializeObject<List<TeamsDTO>>(response.Content.ReadAsStringAsync().Result);

            WriteLine("\nResult:\n");
            if (result.Count > 0)
                result.ForEach(v => WriteLine(v.ToString()));
            else
                WriteLine("Not yet");

            WaitForAnyKeyPress();
        }
    }
}

