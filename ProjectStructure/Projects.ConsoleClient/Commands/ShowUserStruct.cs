﻿using Projects.ConsoleClient.Interfaces;
using static System.Console;
using static Projects.ConsoleClient.Utils.ConsoleUtils;
using static Projects.ConsoleClient.Validators.IdValidator;
using Projects.ConsoleClient.DTO;
using Newtonsoft.Json;
using Projects.ConsoleClient.Decorators;

namespace Projects.ConsoleClient.Commands
{
    class ShowUserStruct : HttpCommand
    {
        public ShowUserStruct(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public ShowUserStruct(IHttpService httpService, string path, int timeout) : base(httpService, path, timeout)
        {
        }

        [TaskCanceledHandler]
        [HttpErrorHandler]
        public override void Execute()
        {
            if (!TryGetIdInput(out int id))
                return;

            var response = Wait(() => Client.Get($"{Path}/{id}", Timeout).GetAwaiter().GetResult());
            if (response != null && !response.IsSuccessStatusCode)
            {
                ShowHttpResponse<HttpErrorResponse>(response);
                return;
            }

            var result = JsonConvert.DeserializeObject<UserStructDTO>(response.Content.ReadAsStringAsync().Result);

            WriteLine(result.ToString());

            WaitForAnyKeyPress();
        }
    }
}
