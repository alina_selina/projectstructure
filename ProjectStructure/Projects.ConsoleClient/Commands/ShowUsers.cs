﻿using Projects.ConsoleClient.Interfaces;
using static System.Console;
using static Projects.ConsoleClient.Utils.ConsoleUtils;
using Projects.ConsoleClient.DTO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Projects.ConsoleClient.Decorators;

namespace Projects.ConsoleClient.Commands
{
    class ShowUsers : HttpCommand
    {
        public ShowUsers(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public ShowUsers(IHttpService httpService, string path, int timeout) : base(httpService, path, timeout)
        {
        }

        [TaskCanceledHandler]
        [HttpErrorHandler]
        public override void Execute()
        {
            var response = Wait(() => Client.Get(Path, Timeout).GetAwaiter().GetResult());
            if (response != null && !response.IsSuccessStatusCode)
            {
                ShowHttpResponse<HttpErrorResponse>(response);
                return;
            }

            var result = JsonConvert.DeserializeObject<List<UsersDTO>>(response.Content.ReadAsStringAsync().Result);

            int count = 0;
            WriteLine("\nResult:\n");
            if (result.Count > 0)

                while (true)
                {
                    Clear();
                    foreach (UsersDTO dto in result.Skip(count).Take(Settings.PageSize))
                        WriteLine(dto.ToString());

                    if (!TryGetNextCount(ref count, Settings.PageSize, result.Count))
                        break;
                }
            else
                WriteLine("Not yet");

            WaitForAnyKeyPress();
        }
    }
}
