﻿using Projects.ConsoleClient.Interfaces;

namespace Projects.ConsoleClient.DTO
{
    public struct Operation
    {
        public string Name { get; set; }
        public ICommand Command { get; set; }

        public Operation(string name, ICommand method)
        {
            Name = name;
            Command = method;
        }
    }
}
