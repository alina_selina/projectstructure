﻿using System;

namespace Projects.ConsoleClient.DTO
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public UserDTO Author { get; set; }
        public TeamDTO Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return $"ProjectId: {Id}, AuthorId: {Author.Id}, TeamId: {Team.Id}, Name: {Name}, Deadline: {Deadline}, CreatedAt: {CreatedAt}";
        }
    }
}
