﻿namespace Projects.ConsoleClient.DTO
{
    public class UserTasksCurrentYearDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return $"Id - {Id}, Name - {Name}";
        }
    }
}
