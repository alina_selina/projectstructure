﻿using System.Collections.Generic;
using System.Linq;

namespace Projects.ConsoleClient.DTO
{
    public class UsersDTO
    {
        public UserDTO User { get; set; }
        public List<TaskDTO> Tasks { get; set; }

        public override string ToString()
        {
            return $"\n{User}\n{string.Join("\n", Tasks.Select(u => u.ToString()))}";
        }
    }
}
