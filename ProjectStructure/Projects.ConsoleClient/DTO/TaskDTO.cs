﻿using CollectionsAndLINQ.Enums;
using System;

namespace Projects.ConsoleClient.DTO
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public UserDTO Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public override string ToString()
        {
            return $"TaskId: {Id}, ProjectId: {ProjectId}, PerformerId: {Performer.Id}, Name: {Name}, State: {State}, CreatedAt: {CreatedAt}";
        }
    }
}
