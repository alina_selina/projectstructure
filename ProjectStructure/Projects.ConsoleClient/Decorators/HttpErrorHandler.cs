﻿using PostSharp.Aspects;
using PostSharp.Serialization;
using System.Net.Http;
using static Projects.ConsoleClient.Utils.ConsoleUtils;

namespace Projects.ConsoleClient.Decorators
{
    [PSerializable]
    public class HttpErrorHandler : MethodInterceptionAspect
    {
        public override void OnInvoke(MethodInterceptionArgs args)
        {
            try
            {
                args.Proceed();
            }
            catch (HttpRequestException ex)
            {
                WriteErrorMessage($"Error: {ex.Message}", true);
                WaitForAnyKeyPress();
            }
        }
    }
}
