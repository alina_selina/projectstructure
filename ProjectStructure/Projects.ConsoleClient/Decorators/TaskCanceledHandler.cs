﻿using static Projects.ConsoleClient.Utils.ConsoleUtils;
using PostSharp.Aspects;
using PostSharp.Serialization;
using System.Threading.Tasks;

namespace Projects.ConsoleClient.Decorators
{
    [PSerializable]
    public class TaskCanceledHandler : MethodInterceptionAspect
    {
        public override void OnInvoke(MethodInterceptionArgs args)
        {
            try
            {
                args.Proceed();
            }
            catch (TaskCanceledException)
            {
                WriteErrorMessage("Timeout", true);
                WaitForAnyKeyPress();
            }
        }
    }
}
