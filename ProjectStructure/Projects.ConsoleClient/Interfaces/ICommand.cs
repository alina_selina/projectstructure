﻿namespace Projects.ConsoleClient.Interfaces
{
    public interface ICommand
    {
        void Execute();
    }
}
