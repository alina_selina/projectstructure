﻿using Projects.ConsoleClient.Commands;
using Projects.ConsoleClient.DTO;
using Projects.ConsoleClient.Services;
using System;
using System.Collections.Generic;
using System.Media;

namespace Projects.ConsoleClient
{
    class Program
    {
        private static Dictionary<int, Operation> _mainMenu;
        private static string _header = "QUERY SERVICE";

        private static HttpService httpService;

        static Program()
        {
            httpService = new HttpService()
            {
                BaseAddress = new Uri(Settings.BaseAddress),
                Timeout = TimeSpan.FromMilliseconds(Settings.Timeout)
            };

            InitializeMenu();
        }

        static void Main()
        {
            StartMenu(_mainMenu, _header);
        }

        static void StartMenu(Dictionary<int, Operation> menu, string header)
        {
            while (true)
            {
                Console.Clear();
                Menu.ShowHeader(header);
                Menu.Show(menu);
                Menu.SelectOperation(menu);
            }
        }

        static void InitializeMenu()
        {
            _mainMenu = new Dictionary<int, Operation>
            {
               {1, new Operation("ShowProjectTaskCountByUserId", new ShowProjectTaskCountByUserId(httpService, "ProjectTaskCount"))},
               {2, new Operation("ShowUserTasksByUserId", new ShowUserTasksByUserId(httpService, "UserTasks"))},
               {3, new Operation("ShowUserTasksCurrentYearByUserId", new ShowUserTasksCurrentYearByUserId(httpService, "UserTasksCurrentYear"))},
               {4, new Operation("ShowTeams", new ShowTeams(httpService, "TeamsByCondition"))},
               {5, new Operation("ShowUsers", new ShowUsers(httpService, "UsersWithTasks"))},
               {6, new Operation("ShowUserStruct", new ShowUserStruct(httpService, "UserStruct"))},
               {7, new Operation("ShowProjectStruct", new ShowProjectStruct(httpService, "ProjectStruct"))},
               {8, new Operation("Quit", new Command(()=>
               {
                   new SoundPlayer($"{Settings.ResoursesPath}that'sAllFolks.wav").PlaySync();
                   Environment.Exit(0);
               })) }
            };
        }
    }
}