﻿using Projects.ConsoleClient.Interfaces;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Projects.ConsoleClient.Services
{
    public class HttpService : IHttpService
    {
        private readonly HttpClient _client = new();

        public Uri BaseAddress
        {
            get => _client.BaseAddress;
            set => _client.BaseAddress = value;
        }

        public TimeSpan Timeout
        {
            get => _client.Timeout;
            set => _client.Timeout = value;
        }

        public Task<HttpResponseMessage> Get(string path, int? timeout = null)
        {
            if (timeout != null)
            {
                using var tokenSource = new CancellationTokenSource();
                tokenSource.CancelAfter((int)timeout);
                return _client.GetAsync(path, tokenSource.Token);
            }

            return _client.GetAsync(path);
        }

        public Task<HttpResponseMessage> Post(string path, HttpContent data, int? timeout = null)
        {
            if (timeout != null)
            {
                using var tokenSource = new CancellationTokenSource();
                tokenSource.CancelAfter((int)timeout);
                return _client.PostAsync(path, data, tokenSource.Token);
            }

            return _client.PostAsync(path, data);
        }

        public Task<HttpResponseMessage> Put(string path, HttpContent data, int? timeout = null)
        {
            if (timeout != null)
            {
                using var tokenSource = new CancellationTokenSource();
                tokenSource.CancelAfter((int)timeout);
                return _client.PutAsync(path, data, tokenSource.Token);
            }

            return _client.PutAsync(path, data);
        }

        public Task<HttpResponseMessage> Delete(string path, int? timeout = null)
        {
            if (timeout != null)
            {
                using var tokenSource = new CancellationTokenSource();
                tokenSource.CancelAfter((int)timeout);
                return _client.DeleteAsync(path, tokenSource.Token);
            }

            return _client.DeleteAsync(path);
        }

        public void Dispose() => _client.Dispose();
    }
}
