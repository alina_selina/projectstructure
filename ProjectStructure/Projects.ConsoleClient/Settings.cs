﻿using System;
using System.IO;
using System.Reflection;

namespace Projects.ConsoleClient
{
    public static class Settings
    {
        public const ConsoleColor ErrorMessageColor = ConsoleColor.Red;
        public const ConsoleColor WarningMessageColor = ConsoleColor.Yellow;

        public const int PageSize = 20;

        public const int Timeout = 60 * 1000;

        public const string BaseAddress = "https://localhost:44313/api/Query/";

        public static readonly string ResoursesPath =
    $@"{Directory.GetParent(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)).Parent.Parent}{Path.DirectorySeparatorChar}Resourses{Path.DirectorySeparatorChar}";
    }
}
