﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using static System.Console;

namespace Projects.ConsoleClient.Utils
{
    public static class ConsoleUtils
    {
        public static void WaitForAnyKeyPress(string message = "\nPress any key to continue...")
        {
            WriteLine(message);
            ReadKey();
        }

        public static void ClearLine()
        {
            SetCursorPosition(0, CursorTop - 1);
            Write(new string(' ', WindowWidth));
            SetCursorPosition(0, CursorTop - 1);
        }

        public static bool IsTryingGetValueAgain(string message)
        {
            while (true)
            {
                ClearLine();
                WriteWarningMessage($"{message} Try again? [y/n]: ");

                var input = ReadLine();
                if (input == "y")
                {
                    ClearLine();
                    return true;
                }
                else if (input == "n")
                {
                    ClearLine();
                    return false;
                }
            }
        }

        public static bool TryGetNextCount(ref int count, int page, int maxCount)
        {
            WriteWarningMessage($"prev page [p] | next page [any key] | quit [q]: ");
            ConsoleKeyInfo key = ReadKey();

            if (key.KeyChar == 'q')
                return false;
            else if (key.KeyChar == 'p')
                count -= count > 0 ? page : 0;
            else
                count += count + page > maxCount ? 0 : page;

            return true;
        }

        public static T Wait<T>(Func<T> func)
        {
            WriteWarningMessage("Loading...", true);
            try
            {
                return func.Invoke();
            }
            finally
            {
                ClearLine();
            }
        }

        public static void WriteWarningMessage(string message, bool newLine = false)
        {
            WriteMessage(message, Settings.WarningMessageColor, newLine);
        }

        public static void WriteErrorMessage(string message, bool newLine = false)
        {
            WriteMessage(message, Settings.ErrorMessageColor, newLine);
        }

        public static void WriteMessage(string message, ConsoleColor color, bool newLine = false)
        {
            ForegroundColor = color;
            if (newLine)
                WriteLine(message);
            else
                Write(message);
            ResetColor();
        }

        public static void ShowHttpResponse<T>(HttpResponseMessage response, string message = "")
        {
            var result = response.Content.ReadAsStringAsync().Result;
            var obj = JsonConvert.DeserializeObject<T>(result);

            WriteLine($"\n{message}\n{obj}");
            WaitForAnyKeyPress();
        }
    }
}
