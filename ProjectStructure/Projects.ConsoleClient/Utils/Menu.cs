﻿using System.Collections.Generic;
using Projects.ConsoleClient.DTO;
using static System.Console;
using static Projects.ConsoleClient.Utils.ConsoleUtils;

namespace Projects.ConsoleClient
{
    public static class Menu
    {
        public static void Show(Dictionary<int, Operation> menu)
        {
            foreach (KeyValuePair<int, Operation> keyValue in menu)
                WriteLine($"{keyValue.Key} - {keyValue.Value.Name}");
        }

        public static void SelectOperation(Dictionary<int, Operation> menu)
        {
            Write("\nSelect operation: ");
            if (!int.TryParse(ReadLine(), out int operation)
               || !menu.TryGetValue(operation, out Operation menuOperation))
            {
                WriteErrorMessage("Incorrect value", true);
                WaitForAnyKeyPress();
                return;
            }
            menuOperation.Command.Execute();
        }

        public static void ShowHeader(string header)
        {
            var line = new string('─', header.Length * 3);
            var spaces = new string(' ', header.Length);
            WriteLine(line);
            WriteLine($"{spaces}{header}");
            WriteLine(line);
            WriteLine();
        }
    }
}
