﻿using static System.Console;
using static Projects.ConsoleClient.Utils.ConsoleUtils;

namespace Projects.ConsoleClient.Validators
{
    public static class IdValidator
    {
        public static bool TryGetIdInput(out int id)
        {
            WriteLine("\nEnter id:");

            while (true)
            {
                if (int.TryParse(ReadLine(), out id))
                    return true;

                else if (!IsTryingGetValueAgain("Incorrect value."))
                {
                    id = 0;
                    return false;
                }
            }
        }
    }
}
