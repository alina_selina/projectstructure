﻿using CollectionsAndLINQ.Entities;
using Projects.Common.Interfaces;
using Projects.DAL.Interfaces;
using System.Collections.Generic;

namespace Projects.DAL.Data
{
    public class DataProvider : IDataProvider
    {
        public List<User> Users { get; set; }
        public List<Project> Projects { get; set; }
        public List<Team> Teams { get; set; }
        public List<Task> Tasks { get; set; }

        public DataProvider()
        {
            Users = new List<User>();
            Projects = new List<Project>();
            Teams = new List<Team>();
            Tasks = new List<Task>();
        }

        public void Dispose()
        {
            Users.Clear();
            Projects.Clear();
            Teams.Clear();
            Tasks.Clear();
        }

        public List<TEntity> SetOf<TEntity>() where TEntity : class, IEntity
        {
            if (Users is List<TEntity>)
                return Users as List<TEntity>;
            else if (Projects is IEnumerable<TEntity>)
                return Projects as List<TEntity>;
            else if (Teams is IEnumerable<TEntity>)
                return Teams as List<TEntity>;
            else if (Tasks is IEnumerable<TEntity>)
                return Tasks as List<TEntity>;

            return null;
        }
    }
}
