﻿using Bogus;
using CollectionsAndLINQ.Entities;
using Projects.Common.Enums;
using Projects.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Projects.DAL.Data
{
    public static class DataProviderExtensions
    {
        private const int EntityCount = 20;

        public static void Seed(this IDataProvider dataProvider)
        {
            var teams = GenerateRandomTeams();
            var users = GenerateRandomUsers(teams);
            var projects = GenerateRandomProjects(users, teams);
            var tasks = GenerateRandomTasks(projects, users);

            teams.ForEach(t => t.Users = users.Where(u => u.TeamId.Equals(t.Id)).ToList());
            projects.ForEach(p => p.Tasks = tasks.Where(t => t.ProjectId.Equals(p.Id)).ToList());

            dataProvider.Users.AddRange(users);
            dataProvider.Teams.AddRange(teams);
            dataProvider.Tasks.AddRange(tasks);
            dataProvider.Projects.AddRange(projects);
        }

        public static List<Project> GenerateRandomProjects(List<User> users, List<Team> teams)
        {
            var rand = new Random();
            int pId = 1;

            var projectsFake = new Faker<Project>()
                .RuleFor(u => u.Id, f => pId++)
                .RuleFor(u => u.Author, f => users.OrderBy(s => rand.NextDouble()).First())
                .RuleFor(u => u.Deadline, f => f.Date.Future())
                .RuleFor(u => u.Description, f => f.Lorem.Sentence())
                .RuleFor(u => u.Name, f => f.Lorem.Word())
                .RuleFor(u => u.Team, f => teams.OrderBy(s => rand.NextDouble()).First());

            var projects = projectsFake.Generate(EntityCount);

            return projects.ToList();
        }

        public static List<Task> GenerateRandomTasks(List<Project> projects, List<User> users)
        {
            var rand = new Random();
            int taskId = 1;

            var tasksFake = new Faker<Task>()
               .RuleFor(pi => pi.Id, f => taskId++)
               .RuleFor(pi => pi.Name, f => f.Lorem.Sentence())
               .RuleFor(pi => pi.Performer, f => users.OrderBy(s => rand.NextDouble()).First())
               .RuleFor(pi => pi.Description, f => f.Lorem.Sentence())
               .RuleFor(pi => pi.CreatedAt, f => f.Date.Past())
               .RuleFor(pi => pi.State, f => Enum.GetValues(typeof(TaskState)).OfType<Enum>().OrderBy(s => rand.NextDouble()).First())
               .RuleFor(pi => pi.ProjectId, f => projects.OrderBy(s => rand.NextDouble()).First()?.Id)
               .RuleFor(pi => pi.FinishedAt, f => f.Date.Between(DateTime.Now.Subtract(new TimeSpan(600, 0, 0, 0)), DateTime.Now));

            var tasks = tasksFake.Generate(EntityCount);

            return tasks.ToList();
        }

        public static List<User> GenerateRandomUsers(List<Team> teams)
        {
            var rand = new Random();
            int userId = 1;

            var testUsersFake = new Faker<User>()
                .RuleFor(u => u.Id, f => userId++)
                .RuleFor(u => u.FirstName, f => f.Person.FirstName)
                .RuleFor(u => u.LastName, f => f.Person.LastName)
                .RuleFor(u => u.Email, f => f.Internet.Email())
                .RuleFor(u => u.RegisteredAt, f => f.Date.Past())
                .RuleFor(u => u.BirthDay, f => f.Date.Past())
                .RuleFor(u => u.TeamId, f => teams.OrderBy(s => rand.NextDouble()).First().Id);

            var generatedUsers = testUsersFake.Generate(EntityCount);

            return generatedUsers.ToList();
        }

        public static List<Team> GenerateRandomTeams()
        {
            int teamId = 1;

            var teamsFake = new Faker<Team>()
               .RuleFor(pi => pi.Id, f => teamId++)
               .RuleFor(pi => pi.Name, f => f.Lorem.Word());

            var teams = teamsFake.Generate(EntityCount);

            return teams.ToList();
        }
    }
}