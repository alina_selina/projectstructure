﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace CollectionsAndLINQ.Entities
{
    public class Project : Entity
    {
        public ICollection<Task> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        private static int currentId;

        public override int GetGeneratedId() => Interlocked.Increment(ref currentId);

        public override string ToString()
        {
            return $"ProjectId: {Id}, AuthorId: {Author.Id}, TeamId: {Team.Id}, Name: {Name}, Deadline: {Deadline}, CreatedAt: {CreatedAt}," +
                $"tasks: {Tasks.Count}";
        }
    }
}
