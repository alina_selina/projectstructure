﻿using Projects.Common.Enums;
using System;
using System.Threading;

namespace CollectionsAndLINQ.Entities
{
    public class Task : Entity
    {
        public int ProjectId { get; set; }
        public User Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime? FinishedAt { get; set; }

        private static int currentId;

        public override int GetGeneratedId() => Interlocked.Increment(ref currentId);

        public override string ToString()
        {
            return $"Task id: {Id}, ProjectId: {ProjectId}, PerformerId: {Performer.Id}, Name: {Name}, State: {State}, CreatedAt: {CreatedAt}";
        }
    }
}
