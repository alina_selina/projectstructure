﻿using System.Collections.Generic;
using System.Threading;

namespace CollectionsAndLINQ.Entities
{
    public class Team : Entity
    {
        public string Name { get; set; }
        public ICollection<User> Users { get; set; }

        private static int currentId;

        public override int GetGeneratedId() => Interlocked.Increment(ref currentId);

        public override string ToString()
        {
            return $"Team id: {Id}, Name: {Name}, CreatedAt: {CreatedAt}";
        }
    }
}
