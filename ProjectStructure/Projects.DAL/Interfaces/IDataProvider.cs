﻿using CollectionsAndLINQ.Entities;
using Projects.Common.Interfaces;
using System;
using System.Collections.Generic;

namespace Projects.DAL.Interfaces
{
    public interface IDataProvider : IDisposable
    {
        public List<User> Users { get; set; }

        List<Project> Projects { get; set; }

        List<Team> Teams { get; set; }

        List<Task> Tasks { get; set; }

        List<TEntity> SetOf<TEntity>() where TEntity : class, IEntity;
    }
}
