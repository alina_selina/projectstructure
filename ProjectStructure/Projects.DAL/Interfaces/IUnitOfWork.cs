﻿using Projects.DAL.Interfaces.Repositories;
using System;

namespace Projects.DAL.Interfaces
{
    public interface IUnitOfWork : IRepositoryFactory, IDisposable
    {
        int SaveChanges();
    }
}
