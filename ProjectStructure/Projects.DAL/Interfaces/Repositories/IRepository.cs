﻿using System;
using System.Collections.Generic;

namespace Projects.DAL.Interfaces.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetFirstOrDefault(Func<TEntity, bool> filter = null);
        TEntity Create(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(object id);
        void Delete(TEntity entity);
    }
}
