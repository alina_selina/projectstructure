﻿using Projects.Common.Interfaces;

namespace Projects.DAL.Interfaces.Repositories
{
    public interface IRepositoryFactory
    {
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity, IIdGeneratable;
    }
}
