﻿using AutoMapper;
using Projects.Common.Interfaces;
using Projects.DAL.Interfaces;
using Projects.DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Projects.DAL.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity, IIdGeneratable
    {
        protected IDataProvider Provider;
        protected IMapper Mapper;
        protected List<TEntity> Set;

        public Repository(IDataProvider provider, IMapper mapper)
        {
            Provider = provider;
            Mapper = mapper;
            Set = Provider.SetOf<TEntity>();
        }

        public virtual TEntity Create(TEntity entity)
        {
            entity.Id = entity.GetGeneratedId();
            Set.Add(entity);
            return entity;
        }

        public void Delete(object id)
        {
            var entityToDelete = Set.Find(e => e.Id.Equals(id));

            if (entityToDelete != null)
                Delete(entityToDelete);
        }

        public void Delete(TEntity entity)
        {
            Set.Remove(entity);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Set;
        }

        public virtual TEntity GetFirstOrDefault(Func<TEntity, bool> filter = null)
        {
            IEnumerable<TEntity> result = Set;

            if (filter != null)
                result = result.Where(filter);

            return result.FirstOrDefault();
        }

        public virtual TEntity Update(TEntity entity)
        {
            var entityToUpdate = Set.FirstOrDefault(e => e.Id.Equals(entity.Id));

            if (entityToUpdate == null)
                return null;

            return Mapper.Map(entity, entityToUpdate);
        }
    }
}