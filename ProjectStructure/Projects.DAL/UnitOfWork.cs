﻿using AutoMapper;
using Projects.Common.Interfaces;
using Projects.DAL.Interfaces;
using Projects.DAL.Interfaces.Repositories;
using Projects.DAL.Repositories;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Projects.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDictionary<Type, object> Repositories { get; }
        private readonly IDataProvider _provider;
        private readonly IMapper _mapper;

        public UnitOfWork(IDataProvider provider, IMapper mapper)
        {
            _provider = provider;
            _mapper = mapper;
            Repositories = new ConcurrentDictionary<Type, object>();
        }

        public void Dispose()
        {
            Repositories.Clear();
            _provider.Dispose();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity, IIdGeneratable
        {
            Type entityType = typeof(TEntity);

            if (Repositories.ContainsKey(entityType))
                return (IRepository<TEntity>)Repositories[entityType];

            Repositories[entityType] = new Repository<TEntity>(_provider, _mapper);
            return (IRepository<TEntity>)Repositories[entityType];
        }

        public int SaveChanges()
        {
            return 1;
        }
    }
}
