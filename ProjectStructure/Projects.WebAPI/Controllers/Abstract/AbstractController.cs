﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Projects.BLL.Exceptions;
using Projects.BLL.Interfaces;
using Projects.WebAPI.Models;
using System.Collections.Generic;
using System.Net;

namespace Projects.WebAPI.Controllers.Abstract
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public abstract class AbstractController<TService, TDto, TCreateDto> : ControllerBase
                                        where TService : IService<TDto, TCreateDto>
                                        where TDto : class where TCreateDto : class
    {
        protected readonly TService _service;
        protected readonly IMapper _mapper;

        protected AbstractController(TService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/EntityName
        [HttpGet]
        public virtual ActionResult<IEnumerable<TDto>> Get()
        {
            return Ok(_service.GetAll());
        }

        // GET: api/EntityName/1
        [HttpGet("{id}")]
        public virtual ActionResult<TDto> GetById(int id)
        {
            try
            {
                return Ok(_service.GetById(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(new HttpErrorResponse(HttpStatusCode.NotFound, ex.Message));
            }
        }

        // POST: api/EntityName
        [HttpPost]
        public virtual ActionResult<TDto> Create([FromBody] TCreateDto dto)
        {
            return Ok(_service.Create(dto));
        }

        // PUT: api/EntityName
        [HttpPut]
        public virtual ActionResult<TDto> Update([FromBody] TDto dto)
        {
            try
            {
                return Ok(_service.Update(dto));
            }
            catch (NotFoundException ex)
            {
                return NotFound(new HttpErrorResponse(HttpStatusCode.NotFound, ex.Message));
            }
        }

        // DELETE: api/EntityName/5
        [HttpDelete("{id}")]
        public virtual ActionResult Delete(int id)
        {
            try
            {
                _service.Delete(id);
                return NoContent();
            }
            catch (NotFoundException ex)
            {
                return NotFound(new HttpErrorResponse(HttpStatusCode.NotFound, ex.Message));
            }
        }
    }
}
