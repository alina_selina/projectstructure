﻿using AutoMapper;
using Projects.WebAPI.Controllers.Abstract;
using Projects.BLL.Interfaces;
using Projects.Common.DTO;

namespace Projects.WebAPI.Controllers
{
    public class ProjectController : AbstractController<IProjectService, ProjectDTO, ProjectCreateDTO>
    {
        public ProjectController(IProjectService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
