﻿using Microsoft.AspNetCore.Mvc;
using Projects.BLL.Interfaces;
using Projects.Common.DTO;
using Projects.Common.DTO.Query;
using Projects.Common.DTO.Task;
using System.Collections.Generic;

namespace Projects.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class QueryController : ControllerBase
    {
        private readonly IQueryService _service;

        public QueryController(IQueryService service)
        {
            _service = service;
        }

        [HttpGet("ProjectTaskCount/{id}")]
        public ActionResult<IEnumerable<ProjectTasksDTO>> GetProjectTaskCount(int id)
        {
            return Ok(_service.GetProjectTaskCountByUserId(id));
        }

        [HttpGet("UserTasks/{id}")]
        public ActionResult<IEnumerable<TaskDTO>> GetUserTasks(int id)
        {
            return Ok(_service.GetUserTasksByUserId(id));
        }

        [HttpGet("UserTasksCurrentYear/{id}")]
        public ActionResult<IEnumerable<UserTasksCurrentYearDTO>> GetUserTasksCurrentYear(int id)
        {
            return Ok(_service.GetUserTasksCurrentYearByUserId(id));
        }

        [HttpGet("TeamsByCondition")]
        public ActionResult<IEnumerable<TeamsDTO>> GetTeams()
        {
            return Ok(_service.GetTeams());
        }

        [HttpGet("UsersWithTasks")]
        public ActionResult<IEnumerable<UsersDTO>> GetUsers()
        {
            return Ok(_service.GetUsers());
        }

        [HttpGet("UserStruct/{id}")]
        public ActionResult<UserStructDTO> GetUserStruct(int id)
        {
            return Ok(_service.GetUserStruct(id));
        }

        [HttpGet("ProjectStruct")]
        public ActionResult<IEnumerable<ProjectStructDTO>> GetProjectStruct()
        {
            return Ok(_service.GetProjectStruct());
        }
    }
}
