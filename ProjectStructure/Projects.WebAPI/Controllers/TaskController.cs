﻿using AutoMapper;
using Projects.WebAPI.Controllers.Abstract;
using Projects.BLL.Interfaces;
using Projects.Common.DTO.Task;

namespace Projects.WebAPI.Controllers
{
    public class TaskController : AbstractController<ITaskService, TaskDTO, TaskCreateDTO>
    {
        public TaskController(ITaskService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
