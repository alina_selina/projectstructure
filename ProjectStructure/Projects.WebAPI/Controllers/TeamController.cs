﻿using AutoMapper;
using Projects.WebAPI.Controllers.Abstract;
using Projects.BLL.Interfaces;
using Projects.Common.DTO.Team;

namespace Projects.WebAPI.Controllers
{
    public class TeamController : AbstractController<ITeamService, TeamDTO, TeamCreateDTO>
    {
        public TeamController(ITeamService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
