﻿using AutoMapper;
using Projects.WebAPI.Controllers.Abstract;
using Projects.BLL.Interfaces;
using Projects.Common.DTO.User;
using Projects.Common.DTO;

namespace Projects.WebAPI.Controllers
{
    public class UserController : AbstractController<IUserService, UserDTO, UserCreateDTO>
    {
        public UserController(IUserService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
