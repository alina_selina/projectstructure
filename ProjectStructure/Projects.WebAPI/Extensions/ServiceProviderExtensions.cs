﻿using Microsoft.Extensions.DependencyInjection;
using Projects.DAL.Data;
using Projects.DAL.Interfaces;
using Projects.BLL.Interfaces;
using Projects.BLL.Services;
using Projects.BLL.MappingProfiles;
using Projects.DAL;
using System.Reflection;

namespace Projects.WebAPI.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static void AddDataProvider(this IServiceCollection services)
        {
            var dataProvider = new DataProvider();
            dataProvider.Seed();

            services.AddSingleton<IDataProvider>(x => dataProvider);
            services.AddSingleton<IUnitOfWork, UnitOfWork>();
        }

        public static void AddCustomServices(this IServiceCollection services)
        {
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IQueryService, QueryService>();
        }

        public static void AddAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}
