﻿using Projects.BLL.Exceptions;
using System.Net;

namespace Projects.WebAPI.Models
{
    public class HttpErrorResponse
    {
        public HttpStatusCode StatusCode { get; set; } = HttpStatusCode.InternalServerError;
        public string Message { get; set; } = "An unexpected error occurred";

        public HttpErrorResponse()
        {
        }

        public HttpErrorResponse(HttpException exception)
        {
            StatusCode = exception.StatusCode;
            Message = exception.Message;
        }

        public HttpErrorResponse(HttpStatusCode statusCode, string message)
        {
            StatusCode = statusCode;
            Message = message;
        }
    }
}
