﻿namespace Projects.WebAPI.Validators
{
    public static class IdValidator
    {
        public static bool IsValid(object id) => id.GetType().Equals(typeof(int));
    }
}
