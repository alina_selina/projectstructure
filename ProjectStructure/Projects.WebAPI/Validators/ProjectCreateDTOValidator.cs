﻿using FluentValidation;
using Projects.Common.DTO;

namespace Projects.WebAPI.Validators
{
    public class ProjectCreateDTOValidator : AbstractValidator<ProjectCreateDTO>
    {
        public ProjectCreateDTOValidator()
        {
            RuleFor(t => t.Name)
                .NotEmpty().WithMessage("Name is mandatory.")
                .MinimumLength(10).WithMessage("Name should be minimum 10 character.");

            RuleFor(t => t.Author)
                .NotEmpty().WithMessage("Author is mandatory.");

            RuleFor(t => t.Team)
                .NotEmpty().WithMessage("Team is mandatory.");

            RuleFor(t => t.Deadline)
                .NotEmpty().WithMessage("Deadline is mandatory.");
        }
    }
}
